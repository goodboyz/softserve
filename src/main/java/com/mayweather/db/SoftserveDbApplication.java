package com.mayweather.db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftserveDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftserveDbApplication.class, args);
	}
}
